import 'package:flutter/material.dart';

const kTextColor = Color(0xFF535353);
const kTextLightColor = Color(0xFFACACAC);

const Color primaryColor = Color(0xFFF67952);
const Color bgColor = Color(0xFFFBFBFD);

const kDefaultPaddin = 20.0;
const double defaultBorderRadius = 12.0;

class ApiConstants {
  static String baseUrl = 'https://2820-197-234-221-78.eu.ngrok.io';
  static String usersEndpoint = '/users';
  static String articleEndpoint = '/article';
  static String loginAndRegisterEndpoint = '/loginAndRegister';
}