import 'package:flutter/material.dart';

class Product {
  final String image, title, description, commune;
  final int price, quantity, id;
  final Color color;
  Product({
    this.id=0,
    this.image="",
    this.title="default",
    this.price=0,
    this.description="",
    this.quantity=0,
    this.color=Colors.green,
    this.commune="",
  });
}

List<Product> products = [
  Product(
      id: 1,
      title: "Pomme juteuse",
      price: 100,
      quantity: 12,
      description: dummyText,
      image: "assets/images/apple.png",
      color: Color(0xFFd3191f),
      commune: "Partout au Bénin"),
  Product(
      id: 2,
      title: "Banana",
      price: 150,
      quantity: 8,
      description: dummyText,
      image: "assets/images/banana.png",
      color: Color(0xFFfce155),
      commune: "Partout au Bénin"),
  Product(
      id: 3,
      title: "Orange sucrée",
      price: 25,
      quantity: 10,
      description: dummyText,
      image: "assets/images/orange.png",
      color: Color(0xFFe59739),
      commune: "Partout au Bénin"),
  Product(
      id: 4,
      title: "Avocat",
      price: 300,
      quantity: 11,
      description: dummyText,
      image: "assets/images/lawyer.png",
      color: Color(0xFFa1b817),
      commune: "Partout au Bénin"),
  Product(
      id: 5,
      title: "Citron",
      price: 25,
      quantity: 12,
      description: dummyText,
      image: "assets/images/lemon.png",
      color: Color(0xFFd2d438),
      commune: "Partout au Bénin"),
  Product(
    id: 6,
    title: "default",
    price: 0,
    quantity: 0,
    description: dummyText,
    image: "assets/images/default.png",
    color: Color(0xFF75973d),
    commune: "default"),
  Product(
    id: 7,
    title: "default",
    price: 0,
    quantity: 0,
    description: dummyText,
    image: "assets/images/default.png",
    color: Color(0xFF75973d),
    commune: "default"),
  Product(
    id: 8,
    title: "default",
    price: 0,
    quantity: 0,
    description: dummyText,
    image: "assets/images/default.png",
    color: Color(0xFF75973d),
    commune: "default"),
  Product(
    id: 9,
    title: "default",
    price: 0,
    quantity: 0,
    description: dummyText,
    image: "assets/images/default.png",
    color: Color(0xFF75973d),
    commune: "default"),
  Product(
    id: 10,
    title: "default",
    price: 0,
    quantity: 0,
    description: dummyText,
    image: "assets/images/default.png",
    color: Color(0xFF75973d),
    commune: "default"),
];

String dummyText =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. When an unknown printer took a galley.";