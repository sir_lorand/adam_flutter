import 'dart:convert';

List<UserModel> userModelFromJson(String str) => List<UserModel>.from(json.decode(str).map((x) => UserModel.fromJson(x)));

String userModelToJson(List<UserModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UserModel {
  UserModel({
    this.firstName,
    this.lastName,
    required this.email,
    this.phoneNumber,
    required this.password,
  });

  String? firstName;
  String? lastName;
  String email;
  String? phoneNumber;
  String password;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    firstName: json["first_name"],
    lastName: json["last_name"],
    email: json["email"],
    phoneNumber: json["phone_number"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "first_name": firstName,
    "last_name": lastName,
    "email": email,
    "phone_number": phoneNumber,
    "password": password,
  };
}
