import 'dart:convert';

List<ArticleModel> articleModelFromJson(String str) => List<ArticleModel>.from(json.decode(str).map((x) => ArticleModel.fromJson(x)));

String articleModelToJson(List<ArticleModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ArticleModel {
  ArticleModel({
    required this.name,
    required this.quantity,
    this.description,
    required this.commune,
    required this.price,
    required this.image,
  });

  String name;
  int quantity;
  String? description;
  String commune;
  int price;
  String image;

  factory ArticleModel.fromJson(Map<String, dynamic> json) => ArticleModel(
    name: json["name"],
    quantity: json["quantity"],
    description: json["description"],
    commune: json["commune"],
    price: json["price"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "quantity": quantity,
    "description": description,
    "commune": commune,
    "price": price,
    "image": image,
  };
}
