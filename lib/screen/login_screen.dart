import 'package:adam/screen/welcome2_page.dart';
import 'package:flutter/material.dart';
import 'package:adam/screen/signup_page.dart';
import 'package:adam/screen/welcome_page.dart';

import '../constants.dart';
import '../models/user_model.dart';
import '../remote_services.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StartState();
}

class StartState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  var user = UserModel(email: '', password: '');
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return initWidget();
  }

  Widget initWidget() {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Color(0xFF5d8a12),
          leading: IconButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute
                (builder: (context) => HomePage()));
            },
            icon: Icon(
              Icons.arrow_back,
              size: 20,
              color: Colors.white
            ),
          ),
        ),
      body: Container(
        decoration: BoxDecoration(
          color: Color(0xFF5d8a12),
          gradient: LinearGradient(
            colors: [(new  Color(0xFF5d8a12)),new  Color(0xFF5d8a12), new Color(0xFF5d8a12), new Color(0xFF5d8a12), new Color(0xFF6d962a), new Color(0xFF7da141)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 180,
                      margin: EdgeInsets.only(top: 0,),
                      child: Image.asset('assets/images/adam.png'),
                    )
                  ),
                ],
              ),
              SizedBox(height: 60,),
              Expanded(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50),
                        )
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(30),
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: 30,),
                          Column(
                              children: <Widget>[
                                inputFile(label: "Email", myController: _emailController),
                                inputFile(label: "Mot de passe", obscureText: true, myController: _passwordController),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Text("Mot de passe oublié?", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 11, color: Color(0xFF5d8a12),),),
                                  ],
                                ),
                              ],
                            ),
                          SizedBox(height: 55,),
                          Container(
                            height: 40,
                            margin: EdgeInsets.symmetric(horizontal: 50),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Color(0xFF7da141),
                            ),
                            child: Center(
                              child: MaterialButton(
                                onPressed: () async {
                                  /*if (_formKey.currentState!.validate()) {
                                    user.email = _emailController.text;
                                    user.password = _passwordController.text;
                                    var response = await BaseClient().post(ApiConstants.loginAndRegisterEndpoint, user).catchError((err) {});
                                    if (response == null) return;
                                    debugPrint('succesful connexion');
                                    const Center(child: CircularProgressIndicator(),);*/
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen2()));
                                  /*}else {
                                    debugPrint('failed connexion');
                                  }*/
                                },
                                child: Text('Se connecter', style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.w600,)),
                              ),
                            ),
                          ),
                          SizedBox(height: 55,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Vous n'avez pas de compte?", style: TextStyle(
                                fontSize: 11,
                              ),),
                              GestureDetector(
                                onTap: () => {
                                  Navigator.push(context, MaterialPageRoute
                                    (builder: (context) => SignupPage())),
                                },
                                child: Text("Inscrivez-vous ici", style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 13,
                                  color: Color(0xFF5d8a12),
                                ),),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  )
              ),
            ],
          ),
        ),
      )
    );
  }
}


// we will be creating a widget for text field
Widget inputFile({label, obscureText = false, myController}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        label,
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
      ),
      SizedBox(
        height: 5,
      ),
      TextFormField(
        controller: myController,
        validator: (value) {
          if(label == "Email") {
            if(value!.isEmpty) {
              return 'Remplisser ce champ';
            }else if(RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
              return null;
            }else {
              return 'Entrer un email valide!';
            }
          }
          else {
            if(value!.isEmpty) {
              return 'Remplisser ce champ';
            }
            return null;
          };
        },
        obscureText: obscureText,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.circular(15),
            ),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey,)
            )
        ),
      ),
      SizedBox(height: 5,)
    ],
  );
}


