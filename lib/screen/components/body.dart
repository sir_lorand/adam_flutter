import 'package:adam/constants.dart';
import 'package:adam/models/product.dart';
import 'package:adam/screen/components/search_form.dart';
import 'package:adam/screen/components/section_title.dart';
import 'package:adam/screen/detail/detail_screen.dart';
import 'package:flutter/material.dart';


import 'categories.dart';
import 'item_card.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
          child: Text(
            "Articles",
            style: Theme
                .of(context)
                .textTheme
                .headline5
                ?.copyWith(fontWeight: FontWeight.bold),
          ),
        ),
        const Padding(
          padding: EdgeInsets.symmetric(vertical: kDefaultPaddin),
          child: SearchForm(),
        ),
        Categories(),
        SizedBox(height: 5,),
        SectionTitle(
          title: "Nouveau arrivage",
          pressSeeAll: () {},
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
            child: GridView.builder(
              itemCount: products.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: kDefaultPaddin,
                crossAxisSpacing: kDefaultPaddin,
                childAspectRatio: 0.75,
              ),
              itemBuilder: (context, index) => ItemCard(
                  product: products[index], 
                  press: () {
                     Navigator.push(
                     context,
                     MaterialPageRoute(
                        builder: (context) => DetailsScreen(
                            product: products[index],
                        ),
                        ));
                  }
                )),
          ),
        ),
      ],
    );
  }
}







