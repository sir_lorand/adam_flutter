import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../constants.dart';

class SearchForm extends StatelessWidget {
  const SearchForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
        child: TextFormField(
          decoration: InputDecoration(
              hintText: "Rechercher un article...",
              filled: true,
              fillColor: Colors.white,
              border:outlineInputBorder,
              enabledBorder: outlineInputBorder,
              focusedBorder: outlineInputBorder,
              prefixIcon: Padding(
                padding: const EdgeInsets.all(12),
                child: SvgPicture.asset("assets/icons/search.svg", color: kTextColor),
              ),
              suffixIcon: Padding(
                padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin / 2),
                child: SizedBox(
                  height: 48,
                  width: 48,
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                        primary: primaryColor,
                        shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(defaultBorderRadius)))
                    ),
                    child: SvgPicture.asset("assets/icons/filter.svg", color: kTextColor),
                  ),
                ),
              )
          ),
        )
    );
  }
}


const OutlineInputBorder outlineInputBorder =  OutlineInputBorder(
  borderRadius:
  BorderRadius.all(Radius.circular(defaultBorderRadius),),
  borderSide: BorderSide.none,
);