import 'dart:async';
import 'package:flutter/material.dart';
import 'package:adam/screen/delayed_animation.dart';
import 'package:adam/screen/welcome_page.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => InitState();
}

class InitState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  startTimer() async {
    var duration = Duration(seconds: 8);
    return new Timer(duration, loginRoute);
  }

  loginRoute() {
    Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context) => HomePage(),
    ));
  }


  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF5d8a12),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(
              vertical: 60,
              horizontal: 30,
            ),
            child: Column(
              children: [
                DelayedAnimation(
                  delay: 2500,
                  child: Container(
                    height: 190,
                    margin: EdgeInsets.only(
                      top: 170,
                      left: 40,
                    ),
                    child: Image.asset('assets/images/adam.png'),
                  ),
                ),
              ],
            ),
          ),
        )
    );
  }
}
