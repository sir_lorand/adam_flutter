import 'package:adam/remote_services.dart';
import 'package:adam/screen/confirmation_screen.dart';
import 'package:flutter/material.dart';
import 'package:adam/screen/login_screen.dart';
import 'package:adam/screen/welcome_page.dart';


import '../constants.dart';
import '../models/user_model.dart';

class SignupPage extends StatefulWidget {
  const SignupPage({Key? key}) : super(key: key);

  @override
  State<SignupPage> createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {

  final _formKey = GlobalKey<FormState>();
  var user = UserModel(firstName: '', lastName: '', email: '', phoneNumber: '', password: '');
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneNumberController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/sign_up.png'),
          ),
        ),
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomePage()));
              },
              icon: Icon(Icons.arrow_back),
            ),
          ),
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 40),
              height: MediaQuery.of(context).size.height - 50,
              width: double.infinity,
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          "Inscription",
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Commencez avec ADAM",
                          style:
                              TextStyle(fontSize: 15, color: Colors.grey[700]),
                        )
                      ],
                    ),
                    SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          inputFile(label: "Nom", myController: _firstNameController),
                          inputFile(label: "Prénom", myController: _lastNameController),
                          inputFile(label: "Email", myController: _emailController),
                          inputFile(label: "Tel", myController: _phoneNumberController),
                          inputFile(label: "Mot de passe", obscureText: true, myController: _passwordController),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 3, left: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: MaterialButton(
                        minWidth: double.infinity,
                        height: 50,
                        onPressed: () async {
                          /*if (_formKey.currentState!.validate()) {
                            user.firstName = _firstNameController.text;
                            user.lastName = _lastNameController.text;
                            user.email = _emailController.text;
                            user.phoneNumber = _phoneNumberController.text;
                            user.password = _passwordController.text;
                            var response = await BaseClient().post(ApiConstants.usersEndpoint, user).catchError((err) {});
                            if (response == null) return;
                            debugPrint('succesful register');
                            const Center(child: CircularProgressIndicator(),);*/
                            Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyEmail()));
                          /*} else {
                            debugPrint('failed register');
                          }*/
                        },
                        color: Color(0xFF7da141),
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        child: Text(
                          "S'inscrire",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Vous avez déja un compte?",
                          style: TextStyle(
                            fontSize: 11,
                          ),
                        ),
                        GestureDetector(
                          onTap: () => {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LoginScreen())),
                          },
                          child: Text(
                            "Connectez-vous ici",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}

// we will be creating a widget for text field
Widget inputFile({label, obscureText = false, myController}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        label,
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
      ),
      SizedBox(
        height: 5,
      ),
      TextFormField(
        controller: myController,
        validator: (value) {
          if(label == "Email") {
            if(value!.isEmpty) {
              return 'Remplisser ce champ';
            }else if(RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
              return null;
            }else {
              return 'Entrer un email valide!';
            }
          }
          else {
            if(value!.isEmpty) {
              return 'Remplisser ce champ';
            }
            return null;
          };
        },
        obscureText: obscureText,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.circular(15),
            ),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey,)
            )
        ),
      ),
      SizedBox(height: 5,)
    ],
  );
}

