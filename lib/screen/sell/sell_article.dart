import 'dart:convert';
import 'dart:ffi';
import 'dart:io';

import 'package:adam/screen/welcome2_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

import '../../constants.dart';
import 'package:http/http.dart' as http;
import '../enums.dart';
import '../profile/components/custom_bottom_nav_bar.dart';
import '../welcome2_page.dart';
import 'package:path/path.dart';
import 'package:http_parser/http_parser.dart';

class SellPage extends StatefulWidget {
  const SellPage({Key? key}) : super(key: key);

  @override
  State<SellPage> createState() => _SellPageState();
}

class _SellPageState extends State<SellPage> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _quantityController = TextEditingController();
  final _priceController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _communeController = TextEditingController();

  File? _imageSend;

  Future getImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image != null){

        final imagePermanent = await saveFilePermanently(image.path);
        setState(() {
          _imageSend = imagePermanent;
        });
      }
    
    } on PlatformException catch (error) {
      print("Echec du chargement de l'image: $error");
    }
  }

  Future<File> saveFilePermanently(String imagePath) async {
    final directory = await getApplicationDocumentsDirectory();
    final name = basename(imagePath);
    final image = File('${directory.path}/$name');

    return File(imagePath).copy(image.path);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/sign_up.png'),
          ),
        ),
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomeScreen2()));
              },
              icon: Icon(Icons.arrow_back),
            ),
          ),
          backgroundColor: Colors.transparent,
          body: Form(
            key: _formKey,
            child: Center(
                child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 40),
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          "Selling page",
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Reinseigner les caratéristiques concernant l'article que vous désirez vendre",
                          textAlign: TextAlign.center,
                          style:
                              TextStyle(fontSize: 15, color: Colors.grey[700]),
                        )
                      ],
                    ),
                    SizedBox(height: 20),
                    Column(
                      children: <Widget>[
                        Column(
                          children: [
                            SizedBox(
                              height: 30,
                            ),
                            _imageSend != null
                                ? Image.file(_imageSend!, width: 180, height: 180, fit: BoxFit.cover,)
                                : Image(height: 180, width: 180, image: AssetImage("assets/images/gallery.jpg")),
                            SizedBox(
                              height: 30,
                            ),
                            CustomButton(
                                title: "Importer l'image de la gallery",
                                icon: Icons.image_outlined,
                                onClick: () => getImage(ImageSource.gallery)),
                            SizedBox(
                              height: 10,
                            ),
                            CustomButton(
                                title: 'Prendre une photo',
                                icon: Icons.camera,
                                onClick: () => getImage(ImageSource.camera)),
                          ],
                        ),
                        SizedBox(height: 20),
                        inputFile(
                            label: "Nom de l'article",
                            myController: _nameController),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Prix",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            TextFormField(
                              controller: _priceController,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Remplisser ce champ';
                                } else if (RegExp(
                                        r'^[0-9]|[1-9][0-9]|[1-9][0-9][0-9]| [1-9][0-9][0-9][ 0-9]+$')
                                    .hasMatch(value)) {
                                  return null;
                                } else {
                                  return 'Entrer un nombre valide!';
                                }
                              },
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 10),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                    color: Colors.grey,
                                  ))),
                            ),
                            SizedBox(
                              height: 5,
                            )
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Quantité",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            TextFormField(
                              controller: _quantityController,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Remplisser ce champ';
                                } else if (RegExp(
                                        r'^[0-9]|[1-9][0-9]|[1-9][0-9][0-9]| [1-9][0-9][0-9][ 0-9]+$')
                                    .hasMatch(value)) {
                                  return null;
                                } else {
                                  return 'Entrer un nombre valide!';
                                }
                              },
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 10),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                    color: Colors.grey,
                                  ))),
                            ),
                            SizedBox(
                              height: 5,
                            )
                          ],
                        ),
                        inputFile(
                            label: "Description",
                            myController: _descriptionController),
                        inputFile(
                            label: "Commune", myController: _communeController),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 3, left: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: MaterialButton(
                        minWidth: double.infinity,
                        height: 50,
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            _uploadImage(_imageSend!);
                            debugPrint('succès de la publication');
                            const Center(child: CircularProgressIndicator(),);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen2()));
                          } else {
                            print("Pas encore bien");
                          }
                        },
                        color: Color(0xFF7da141),
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        child: Text(
                          "Publier",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )),
          ),
          bottomNavigationBar: CustomBottomNavBar(
            selectedMenu: MenuState.profile,
          ),
        ));
  }

  //============================================================= API Area to upload image
  Uri apiUrl = Uri.parse(ApiConstants.baseUrl + ApiConstants.articleEndpoint);

  Future<Map<String, dynamic>?> _uploadImage(File image) async {
    http.MultipartRequest imageUploadRequest =
        new http.MultipartRequest('POST', apiUrl);

    imageUploadRequest.files.add(http.MultipartFile.fromString('image', image.path,  filename: image.path.split('/').last, contentType: MediaType('image', 'png')));
    imageUploadRequest.fields['name'] = _nameController.text;
    imageUploadRequest.fields['quantity'] = _quantityController.text;
    imageUploadRequest.fields['description'] = _descriptionController.text;
    imageUploadRequest.fields['commune'] = _communeController.text;
    imageUploadRequest.fields['price'] = _priceController.text;

    http.StreamedResponse response = await imageUploadRequest.send();
    print(response.stream.toString());
    if (response.statusCode == 201) {
      print('image uploaded');
      CupertinoAlertDialog(
          title: Text("Publication d'aticle"),
          content: Text('Succes'),
        );
    } else {
      print('failed');
      CupertinoAlertDialog(
        title: Text("Publication d'aticle"),
        content: Text('Echec, vérifié si tous les champs sont bien remplis'),
      );
    }
  }


}

// we will be creating a widget for text field
Widget inputFile({label, obscureText = false, myController}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        label,
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
      ),
      SizedBox(
        height: 5,
      ),
      TextFormField(
        controller: myController,
        validator: (value) {
          if (value!.isEmpty || !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
            return 'Remplisser ce champ';
          }
          return null;
        },
        obscureText: obscureText,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.circular(15),
            ),
            border: OutlineInputBorder(
                borderSide: BorderSide(
              color: Colors.grey,
            ))),
      ),
      SizedBox(
        height: 5,
      )
    ],
  );
}

Widget CustomButton({
  required String title,
  required IconData icon,
  required VoidCallback onClick,
}) {
  return Container(
    width: 280,
    child: ElevatedButton(
        onPressed: onClick,
        child: Row(
          children: [Icon(icon), SizedBox(width: 20), Text(title)],
        )),
  );
}
