import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileMenu extends StatelessWidget {
  const ProfileMenu({
    Key? key,
    required this.icon,
    required this.text,
    required this.press,
  }) : super(key: key);

  final String text, icon;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child:
          SizedBox(
            height: 50,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Color(0xFFF5F6F9),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15))
                ),
                onPressed: press,
                child: Row(
                  children: [
                    SvgPicture.asset(
                      icon,
                      width: 22,
                      color: Colors.green,
                    ),
                    SizedBox(width: 20),
                    Expanded(
                        child: Text(
                          text,
                          style: Theme.of(context).textTheme.bodyText2,
                        )
                    ),
                    Icon(Icons.arrow_forward, color: Colors.black,)
                  ],
                )
            ),
          )
    );
  }
}

