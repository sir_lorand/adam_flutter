import 'package:adam/screen/profile_page.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../models/user_model.dart';
import '../../../remote_services.dart';
import '../../welcome_page.dart';
import 'profil_pic.dart';
import 'profile_menu.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var user = UserModel(firstName: '', lastName: '', email: '', phoneNumber: '', password: '');
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          children: [
            ProfilPic(),
            SizedBox(height: 20),
            ProfileMenu(
              icon: "assets/icons/User Icon.svg",
              text: "Mon compte",
              press: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => EditProfile()));
              },
            ),
            ProfileMenu(
              icon: "assets/icons/Bell.svg",
              text: "Notifications",
              press: () {},
            ),
            ProfileMenu(
              icon: "assets/icons/Settings.svg",
              text: "Paramètres",
              press: () {},
            ),
            ProfileMenu(
              icon: "assets/icons/Question mark.svg",
              text: "Aide",
              press: () {},
            ),
            ProfileMenu(
              icon: "assets/icons/Log out.svg",
              text: "Déconnexion",
              press: () async {
                //var response = await BaseClient().put(ApiConstants.usersEndpoint, user).catchError((err) {});
                Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
              },
            )
          ],
        )
    );
  }
}
