import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../constants.dart';
import '../../enums.dart';
import '../../sell/sell_article.dart';
import '../../welcome2_page.dart';
import '../profile_screen.dart';

class CustomBottomNavBar extends StatelessWidget {
  const CustomBottomNavBar({
    Key? key,
    required this.selectedMenu
  }) : super(key: key);

  final MenuState selectedMenu;

  @override
  Widget build(BuildContext context) {
    final Color inActiveIconColor = Colors.green;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 14),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
            topRight: Radius.circular(40),
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -15),
              blurRadius: 20,
              color: Color(0xFFDADADA).withOpacity(0.15),
            )
          ]
      ),
      child: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen2()));
                },
                icon: SvgPicture.asset("assets/icons/Shop Icon.svg", color: MenuState.home == selectedMenu ? inActiveIconColor : kTextColor)
            ),
            IconButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) =>SellPage()));
                },
                icon: SvgPicture.asset("assets/icons/cart.svg", color: MenuState.sell == selectedMenu ? inActiveIconColor : kTextColor)
            ),
            IconButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileScreen()));
                },
                icon: SvgPicture.asset("assets/icons/User Icon.svg", color: MenuState.profile == selectedMenu ? inActiveIconColor : kTextColor)
            ),
          ],
        ),
      ),
    );
  }
}
