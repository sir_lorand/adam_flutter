import 'package:adam/screen/enums.dart';
import 'package:flutter/material.dart';
import '../welcome2_page.dart';
import 'components/body.dart';
import 'components/custom_bottom_nav_bar.dart';


class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute
              (builder: (context) => HomeScreen2()));
          },
          icon: Icon(
              Icons.arrow_back,
              size: 20,
              color: Colors.black
          ),
        ),
        title: Text(
            "Profile",
            style: TextStyle(
              color: Colors.black
            ),
        ),
      ),
      body: Body(),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.profile,),
    );
  }
}

