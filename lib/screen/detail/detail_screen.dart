import 'package:adam/constants.dart';
import 'package:flutter/material.dart';
import 'package:adam/models/product.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:adam/screen/detail/components/body.dart';


class DetailsScreen extends StatelessWidget {
  final Product product;

  const DetailsScreen({super.key, required this.product});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: product.color,
      appBar: buildAppBar(context),
      body: Body(product: product)
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: product.color,
      elevation: 0,
      leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: SvgPicture.asset("assets/icons/back.svg", color: Colors.black,),
      ),
      actions: <Widget>[
        IconButton(
            onPressed: () {},
            icon: SvgPicture.asset("assets/icons/cart.svg", color: Colors.black,),
        ),
        SizedBox(width: kDefaultPaddin / 2)
      ],
    );
  }
}