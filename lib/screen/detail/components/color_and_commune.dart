import 'package:adam/constants.dart';
import 'package:adam/models/product.dart';
import 'package:flutter/material.dart';

class ColorAndCommune extends StatelessWidget {
  const ColorAndCommune({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Couleur"),
              Row(
                children: <Widget>[
                  ColorDot(color: Colors.green, isSelected: true,),
                  ColorDot(color: Colors.red,),
                  ColorDot(color: Colors.yellow,),
                ],
              )
            ],
          ),
        ),
        Expanded(
            child: RichText(
                text: TextSpan(
                    style: TextStyle(color: kTextColor),
                    children: [
                      TextSpan(text: "Commune\n"),
                      TextSpan(
                          text: "${product.commune}",
                          style:
                          Theme.of(context)
                              .textTheme
                              .headline6
                              ?.copyWith(fontWeight: FontWeight.bold)
                      )
                    ]
                )
            )
        ),
      ],
    );
  }
}

class ColorDot extends StatelessWidget {
  final Color color;
  final bool isSelected;
  const ColorDot({
    Key? key,
    required this.color,
    //par defaut isSelected est faux
    this.isSelected = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: kDefaultPaddin / 4,
        right: kDefaultPaddin / 2,
      ),
      padding: EdgeInsets.all(2.5),
      height: 24,
      width: 24,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            color: isSelected ? color : Colors.transparent,
          )
      ),
      child: DecoratedBox(
          decoration: BoxDecoration(
              color: color,
              shape: BoxShape.circle
          )
      ),
    );
  }
}
