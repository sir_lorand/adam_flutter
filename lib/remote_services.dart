import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:adam/constants.dart';
import 'package:adam/models/user_model.dart';
import 'package:http/http.dart' as http;

class BaseClient {
  var client = http.Client();

  //GET
  Future<List<UserModel>?> get(String api) async {
    try {
      var url = Uri.parse(ApiConstants.baseUrl + api);
      Map<String, String> headers = {'Content-Type': 'application/json'};

      var response = await http.get(url, headers: headers);
      if (response.statusCode == 200) {
        var jsonResponse = json.decode(response.body);
        var data = jsonResponse['record'];
        return data
                .map((user) => new UserModel.fromJson(user))
                .toList();
      }
    } catch (e) {
      log(e.toString());
    }
  }

  //POST
  Future<dynamic> post(String api, dynamic object) async {
    var url = Uri.parse(ApiConstants.baseUrl + api);
    var _payload = json.encode(object);
    var _headers = {
      'Content-Type': 'application/json',
    };

    var response = await client.post(url, body: _payload, headers: _headers);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      //throw exception and catch it in UI
    }
  }

  //PUT
  Future<dynamic> put(String api, dynamic object) async {
    var url = Uri.parse(ApiConstants.baseUrl + api);
    var _payload = json.encode(object);
    var _headers = {
      'Content-Type': 'application/json',
    };

    var response = await client.put(url, body: _payload, headers: _headers);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      //throw exception and catch it in UI
    }
  }

  //DELETE
  Future<dynamic> delete(String api) async {
    var url = Uri.parse(ApiConstants.baseUrl + api);

    var response = await client.delete(url);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      //throw exception and catch it in UI
    }
  }
}
